core = 7.x
api = 2

; uw_admissions
projects[uw_admissions][type] = "module"
projects[uw_admissions][download][type] = "git"
projects[uw_admissions][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_admissions.git"
projects[uw_admissions][download][tag] = "7.x-2.1.1"
projects[uw_admissions][subdir] = ""

; uw_campus_tour_registration
projects[uw_campus_tour_registration][type] = "module"
projects[uw_campus_tour_registration][download][type] = "git"
projects[uw_campus_tour_registration][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_campus_tour_registration.git"
projects[uw_campus_tour_registration][download][tag] = "7.x-3.4"
projects[uw_campus_tour_registration][subdir] = ""

; uw_highschool_visits
projects[uw_highschool_visits][type] = "module"
projects[uw_highschool_visits][download][type] = "git"
projects[uw_highschool_visits][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_highschool_visits.git"
projects[uw_highschool_visits][download][branch] = "master"
projects[uw_highschool_visits][download][revision] = "455a89f"
projects[uw_highschool_visits][subdir] = ""

; uw_mur_newsletter
projects[uw_mur_newsletter][type] = "module"
projects[uw_mur_newsletter][download][type] = "git"
projects[uw_mur_newsletter][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_mur_newsletter.git"
projects[uw_mur_newsletter][download][tag] = "7.x-2.3"
projects[uw_mur_newsletter][subdir] = ""

; uw_request_brochure
projects[uw_request_brochure][type] = "module"
projects[uw_request_brochure][download][type] = "git"
projects[uw_request_brochure][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_request_brochure.git"
projects[uw_request_brochure][download][tag] = "7.x-1.6.1"
projects[uw_request_brochure][subdir] = ""

; uw_studentbudget_calculator
projects[uw_studentbudget_calculator][type] = "module"
projects[uw_studentbudget_calculator][download][type] = "git"
projects[uw_studentbudget_calculator][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_studentbudget_calculator.git"
projects[uw_studentbudget_calculator][download][tag] = "7.x-2.3"
projects[uw_studentbudget_calculator][subdir] = ""

; uw_admissions_programs
projects[uw_admissions_programs][type] = "module"
projects[uw_admissions_programs][download][type] = "git"
projects[uw_admissions_programs][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_admissions_programs.git" 
projects[uw_admissions_programs][download][tag] = "7.x-1.0.3"
projects[uw_admissions_programs][subdir] = ""